<?php

namespace Orh\Tapd\Modules;

class Workspace extends Base
{
    /**
     * 添加公司成员到指定项目.
     *
     * @url https://www.tapd.cn/help/view#1120003271001001484
     */
    public function addMemberByNick(array $data): array
    {
        $uri = 'workspaces/add_member_by_nick';

        $rules = [
            'project_id',
            'workspace_id',
            'nick',
        ];
        $this->validate($data, $rules);

        return $this->http->post($uri, $data);
    }

    /**
     * 获取公司下或者项目下成员.
     *
     * @url https://www.tapd.cn/help/view#1120003271001003132
     */
    public function users(array $query): array
    {
        $uri = 'workspaces/users';

        $rules = [
            'workspace_id',
        ];
        $this->validate($query, $rules);

        return $this->http->get($uri, $query);
    }

    /**
     * 获取公司项目列表.
     *
     * @url https://www.tapd.cn/help/view#1120003271001003174
     */
    public function projects(array $query): array
    {
        $uri = 'workspaces/projects';

        $rules = [
            'company_id',
        ];
        $this->validate($query, $rules);

        return $this->http->get($uri, $query);
    }

    /**
     * 获取用户参与的项目列表
     *
     * @url https://www.tapd.cn/help/show#1120003271001000739
     * @param array $query
     * @return array
     */
    public function userProjects(array $query) : array
    {
        $uri = 'workspaces/user_participant_projects';
        $rules = [
            'nick', 'company_id'
        ];
        $this->validate($query, $rules);

        return $this->http->get($uri, $query);
    }

    /**
     * 获取项目自定义字段配置
     *
     * @url https://www.tapd.cn/help/show#1120003271001000865
     * @param array $query
     * @return array
     */
    public function workspaceCustomFieldSettings(array $query) : array
    {
        $uri = 'workspaces/workspace_custom_field_settings';
        $rules = [
            'workspace_id'
        ];
        $this->validate($query, $rules);

        return $this->http->get($uri, $query);
    }

    /**
     * 获取项目详细信息
     *
     * @url https://o.tapd.cn/document/api-doc/API%E6%96%87%E6%A1%A3/api_reference/workspace/get_workspace_info.html
     * @param array $query
     * @return array
     */
    public function info(array $query)
    {
        $uri = 'workspaces/get_workspace_info';
        $rules = [
            'workspace_id'
        ];
        $this->validate($query, $rules);

        return $this->http->get($uri, $query);
    }
}
